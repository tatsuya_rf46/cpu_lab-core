module top_wrapper(
    input wire CLK_125MHZ_P,
    input wire CLK_125MHZ_N,
    input wire USB_UART_RX,
    output wire USB_UART_TX,
    input wire CPU_RESET
    );

    wire clk, locked;
    clk_wiz cw(
        .reset(CPU_RESET),
        .clk_in1_p(CLK_125MHZ_P),
        .clk_in1_n(CLK_125MHZ_N),
        .clk_out1(clk),
        .locked(locked)
    );

    wire [31:0] v0_data; // debug

    top dut(clk, ~locked, USB_UART_RX, USB_UART_TX);
endmodule
