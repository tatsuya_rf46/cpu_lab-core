module data_mem (clk, we, rst, addr, di, dout);
    input clk;
    input we;
    input rst;
    input [31:0] addr;
    input [31:0] di;
    output [31:0] dout;

    (* ram_style = "BLOCK" *) reg [31:0] RAM [500000:0];
    reg [31:0] dout;

    initial begin
        $readmemb("mytest2_data.mem", RAM, 0, 500000);
    end
    
    always @(posedge clk)
    begin
        if (we) //write enable
            RAM[{2'b00, addr[31:2]}] <= di;
        if (rst) //optional reset
            dout <= 0;
        else
            dout <= RAM[{2'b00, addr[31:2]}];
    end
endmodule
