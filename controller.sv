module controller(
    input  logic       clk, reset,
    input  logic [5:0] op, funct,
    input  logic [4:0] fmt,
    input  logic       zero,
    input  logic       rx_ready, tx_busy,
    input  logic       ccrd, ccrdtoggle_activelow,
    output logic       pcen, memwrite, irwrite, regwrite,
    output logic [1:0] alusrca,
    output logic [2:0] memtoreg,
    output logic [1:0] regdst, pcsrc,
    output logic [2:0] alusrcb,
    output logic [3:0] alucontrol,
    output logic       tx_start,
    output logic       iorf,
    output logic       fregwrite, fregdst, ccwrite, borc,
    output logic [1:0] memtofreg,
    output logic [3:0] fpucontrol
);

    logic branchs, zerotoggle, branch, pcwrite;

    // Decoder.
    decoder dc(clk, reset, op, funct, fmt,
        rx_ready, tx_busy,
        pcwrite, memwrite, irwrite, regwrite,
        alusrca, branch, zerotoggle, 
        memtoreg, regdst, pcsrc, 
        alusrcb, alucontrol,
        tx_start,
        iorf,
        fregwrite, fregdst, ccwrite, borc, branchs,
        memtofreg,
        fpucontrol
    );

    assign pcen = pcwrite | (branch & (zerotoggle ^ zero)) | (branchs & (~ccrdtoggle_activelow ^ ccrd));
endmodule

module decoder(
    input  logic       clk, reset, 
    input  logic [5:0] op, funct,
    input  logic [4:0] fmt,
    input  logic       rx_ready, tx_busy,
    output logic       pcwrite, memwrite, irwrite, regwrite,
    output logic [1:0] alusrca,
    output logic       branch, zerotoggle,
    output logic [2:0] memtoreg,
    output logic [1:0] regdst, pcsrc,
    output logic [2:0] alusrcb,
    output logic [3:0] alucontrol,
    output logic       tx_start,
    output logic       iorf,
    output logic       fregwrite, fregdst, ccwrite, borc, branchs,
    output logic [1:0] memtofreg,
    output logic [3:0] fpucontrol
);

    parameter   FETCH   = 6'b000000;
    parameter   DECODE  = 6'b000001;
    parameter   MEMADR  = 6'b000010;
    parameter   MEMRD   = 6'b000011;
    parameter   MEMWB   = 6'b000100;
    parameter   MEMWR   = 6'b000101;
    parameter   RTYPEEX = 6'b000110;
    parameter   RTYPEWB = 6'b000111;
    parameter   BEQEX   = 6'b001000;
    parameter   BNQEX   = 6'b001001;
    parameter   ADDIEX  = 6'b001010;
    parameter   ADDIWB  = 6'b001011;
    parameter   LUIEX   = 6'b001100;
    parameter   LUIWB   = 6'b001101;
    parameter   ORIEX   = 6'b001110;
    parameter   ORIWB   = 6'b001111;
    parameter   SLTIEX  = 6'b010000;
    parameter   SLTIWB  = 6'b010001;
    parameter   JEX     = 6'b010010;
    parameter   JALEX   = 6'b010011;
    parameter   INWAIT  = 6'b010100;
    parameter   INREADY = 6'b010101;
    parameter   OUTSTART= 6'b010110;
    parameter   OUTEX   = 6'b010111;
    parameter   BSEX    = 6'b011000;
    parameter   ADDSEX  = 6'b011001;
    parameter   ADDSWB  = 6'b011010;
    parameter   SUBSEX  = 6'b011011;
    parameter   SUBSWB  = 6'b011100;
    parameter   MULSEX  = 6'b011101;
    parameter   MULSWB  = 6'b011110;
    parameter   DIVSEX0 = 6'b011111;
    parameter   DIVSEX1 = 6'b100000;
    parameter   DIVSEX2 = 6'b100001;
    parameter   DIVSWB  = 6'b100010;
    parameter   MOVSEX  = 6'b100011;
    parameter   MOVSWB  = 6'b100100;
    parameter   NEGSEX  = 6'b100101;
    parameter   NEGSWB  = 6'b100110;
    parameter   ABSSEX  = 6'b100111;
    parameter   ABSSWB  = 6'b101000;
    parameter   SQRTSEX0= 6'b101001;
    parameter   SQRTSEX1= 6'b101010;
    parameter   SQRTSWB = 6'b101011;
    parameter   CEQSEX  = 6'b101100;
    parameter   CLTSEX  = 6'b101101;
    parameter   MEMWBS  = 6'b101110;
    parameter   MEMWRS  = 6'b101111;
    parameter   FTOIEX  = 6'b110000;
    parameter   FTOIWB  = 6'b110001;
    parameter   ITOFEX  = 6'b110010;
    parameter   ITOFWB  = 6'b110011;

    parameter   LW      = 6'b100011; // Opcode for lw
    parameter   SW      = 6'b101011; // Opcode for sw
    parameter   RTYPE   = 6'b000000; // Opcode for R-type
    parameter   BEQ     = 6'b000100; // Opcode for beq
    parameter   BNQ     = 6'b000101; // Opcode for bnq
    parameter   ADDI    = 6'b001000; // Opcode for addi
    parameter   LUI     = 6'b001111; // Opcode for lui
    parameter   ORI     = 6'b001101; // Opcode for lui
    parameter   SLTI    = 6'b001010; // Opcode for slti
    parameter   J       = 6'b000010; // Opcode for j
    parameter   JAL     = 6'b000011; // Opcode for jal
    parameter   IN      = 6'b011010; // Opcode for in
    parameter   OUT     = 6'b011011; // Opcode for out
    parameter   FRTYPE  = 6'b010001; // Opcode for FR-type
    parameter   LWS     = 6'b110001; // Opcode for lw.s
    parameter   SWS     = 6'b111001; // Opcode for sw.s
    parameter   FTOI    = 6'b111000; // Opcode for ftoi
    parameter   ITOF    = 6'b110000; // Opcode for itof
  
    logic [5:0]  state, nextstate;
    logic [34:0] controls;
  
    // state register
    always_ff @(posedge clk)			
        if(reset) state <= FETCH;
        else state <= nextstate;

    // next state logic
    always_comb
        case(state)
            FETCH:    nextstate = DECODE;
            DECODE: case(op)
                LW:      nextstate = MEMADR;
                SW:      nextstate = MEMADR;
                RTYPE:   nextstate = RTYPEEX;
                BEQ:     nextstate = BEQEX;
                BNQ:     nextstate = BNQEX;
                ADDI:    nextstate = ADDIEX;
                LUI:     nextstate = LUIEX;
                ORI:     nextstate = ORIEX;
                SLTI:    nextstate = SLTIEX;
                J:       nextstate = JEX;
                JAL:     nextstate = JALEX;
                IN:      nextstate = INWAIT;
                OUT:     nextstate = OUTSTART;
                FRTYPE: case(fmt)
                    5'b01000:      nextstate = BSEX;
                    default: case(funct)
                        6'b000000: nextstate = ADDSEX;
                        6'b000001: nextstate = SUBSEX;
                        6'b000010: nextstate = MULSEX;
                        6'b000011: nextstate = DIVSEX0;
                        6'b000110: nextstate = MOVSEX;
                        6'b000111: nextstate = NEGSEX;
                        6'b000101: nextstate = ABSSEX;
                        6'b000100: nextstate = SQRTSEX0;
                        6'b110010: nextstate = CEQSEX;
                        6'b111100: nextstate = CLTSEX;
                        default:   nextstate = 6'bx;
                    endcase
                endcase
                LWS:     nextstate = MEMADR;
                SWS:     nextstate = MEMADR;
                FTOI:    nextstate = FTOIEX;
                ITOF:    nextstate = ITOFEX;
                default: nextstate = 6'bx; // should never happen
            endcase
            MEMADR: case(op)
                SW:      nextstate = MEMWR;
                SWS:     nextstate = MEMWRS;
                // LW and LWS
                default: nextstate = MEMRD;
            endcase
            MEMRD: case(op)
                LW:      nextstate = MEMWB;
                // LWS
                default: nextstate = MEMWBS;
            endcase
            MEMWB:    nextstate = FETCH;
            MEMWR:    nextstate = FETCH;
            RTYPEEX:  nextstate = RTYPEWB;
            RTYPEWB:  nextstate = FETCH;
            BEQEX:    nextstate = FETCH;
            BNQEX:    nextstate = FETCH;
            ADDIEX:   nextstate = ADDIWB;
            ADDIWB:   nextstate = FETCH;
            LUIEX:    nextstate = LUIWB;
            LUIWB:    nextstate = FETCH;
            ORIEX:    nextstate = ORIWB;
            ORIWB:    nextstate = FETCH;
            SLTIEX:   nextstate = SLTIWB;
            SLTIWB:   nextstate = FETCH;
            JEX:      nextstate = FETCH;
            JALEX:    nextstate = FETCH;
            INWAIT: case(rx_ready)
                1'b0: nextstate = INWAIT;
                1'b1: nextstate = INREADY;
            endcase
            INREADY:  nextstate = FETCH;
            OUTSTART: nextstate = OUTEX;
            OUTEX: case(~tx_busy)
                1'b0: nextstate = OUTEX;
                1'b1: nextstate = FETCH;
            endcase
            BSEX:     nextstate = FETCH;
            ADDSEX:   nextstate = ADDSWB;
            ADDSWB:   nextstate = FETCH;
            SUBSEX:   nextstate = SUBSWB;
            SUBSWB:   nextstate = FETCH;
            MULSEX:   nextstate = MULSWB;
            MULSWB:   nextstate = FETCH;
            DIVSEX0:  nextstate = DIVSEX1;
            DIVSEX1:  nextstate = DIVSEX2;
            DIVSEX2:  nextstate = DIVSWB;
            DIVSWB:   nextstate = FETCH;
            MOVSEX:   nextstate = MOVSWB;
            MOVSWB:   nextstate = FETCH;
            NEGSEX:   nextstate = NEGSWB;
            NEGSWB:   nextstate = FETCH;
            ABSSEX:   nextstate = ABSSWB;
            ABSSWB:   nextstate = FETCH;
            SQRTSEX0: nextstate = SQRTSEX1;
            SQRTSEX1: nextstate = SQRTSWB;
            SQRTSWB:  nextstate = FETCH;
            CEQSEX:   nextstate = FETCH;
            CLTSEX:   nextstate = FETCH;
            MEMWBS:   nextstate = FETCH;
            MEMWRS:   nextstate = FETCH;
            FTOIEX:   nextstate = FTOIWB;
            FTOIWB:   nextstate = FETCH;
            ITOFEX:   nextstate = ITOFWB;
            ITOFWB:   nextstate = FETCH;
            default:  nextstate = 6'bx; // should never happen
      endcase

    // output logic
    assign {
        pcwrite, memwrite, irwrite, regwrite, 
        alusrca,
        branch, zerotoggle,
        memtoreg,
        regdst, pcsrc,
        alusrcb,
        alucontrol,
        tx_start,
        iorf, fregwrite, fregdst, ccwrite, borc, branchs,
        memtofreg,
        fpucontrol
    } = controls;

    always_comb
        case(state)
            FETCH:   controls = {4'b1010, 2'b00, 2'b00, 3'b000, 4'b0000, 3'b001, 4'b0010, 1'b0, 6'b000000, 2'b00, 4'b0000};
            DECODE:  controls = {4'b0000, 2'b00, 2'b00, 3'b000, 4'b0000, 3'b011, 4'b0010, 1'b0, 6'b000000, 2'b00, 4'b0000};
            MEMADR:  controls = {4'b0000, 2'b01, 2'b00, 3'b000, 4'b0000, 3'b010, 4'b0010, 1'b0, 6'b000000, 2'b00, 4'b0000};
            MEMRD:   controls = {4'b0000, 2'b00, 2'b00, 3'b000, 4'b0000, 3'b000, 4'b0010, 1'b0, 6'b000000, 2'b00, 4'b0000};
            MEMWB:   controls = {4'b0001, 2'b00, 2'b00, 3'b001, 4'b0000, 3'b000, 4'b0010, 1'b0, 6'b000000, 2'b00, 4'b0000};
            MEMWR:   controls = {4'b0100, 2'b00, 2'b00, 3'b000, 4'b0000, 3'b000, 4'b0010, 1'b0, 6'b000000, 2'b00, 4'b0000};
            RTYPEEX: case(funct)
                6'b100000: controls = {4'b0000, 2'b01, 2'b00, 3'b000, 4'b0000, 3'b000, 4'b0010, 1'b0, 6'b000000, 2'b00, 4'b0000}; // ADD 
                6'b100010: controls = {4'b0000, 2'b01, 2'b00, 3'b000, 4'b0000, 3'b000, 4'b1010, 1'b0, 6'b000000, 2'b00, 4'b0000}; // SUB
                6'b100100: controls = {4'b0000, 2'b01, 2'b00, 3'b000, 4'b0000, 3'b000, 4'b0000, 1'b0, 6'b000000, 2'b00, 4'b0000}; // AND
                6'b100101: controls = {4'b0000, 2'b01, 2'b00, 3'b000, 4'b0000, 3'b000, 4'b0001, 1'b0, 6'b000000, 2'b00, 4'b0000}; // OR
                6'b000000: controls = {4'b0000, 2'b10, 2'b00, 3'b000, 4'b0000, 3'b000, 4'b0100, 1'b0, 6'b000000, 2'b00, 4'b0000}; // SLL
                6'b000010: controls = {4'b0000, 2'b10, 2'b00, 3'b000, 4'b0000, 3'b000, 4'b0101, 1'b0, 6'b000000, 2'b00, 4'b0000}; // SRL
                6'b101010: controls = {4'b0000, 2'b01, 2'b00, 3'b000, 4'b0000, 3'b000, 4'b1011, 1'b0, 6'b000000, 2'b00, 4'b0000}; // SLT
                6'b001000: controls = {4'b0000, 2'b01, 2'b00, 3'b000, 4'b0000, 3'b000, 4'b0010, 1'b0, 6'b000000, 2'b00, 4'b0000}; // JR
                6'b001001: controls = {4'b0000, 2'b01, 2'b00, 3'b000, 4'b0000, 3'b000, 4'b0010, 1'b0, 6'b000000, 2'b00, 4'b0000}; // JALR
                default:   controls = {4'b0000, 2'b01, 2'b00, 3'b000, 4'b0000, 3'b000, 4'bxxxx, 1'b0, 6'b000000, 2'b00, 4'b0000}; // ???
            endcase
            RTYPEWB: case(funct)
                6'b001000: controls = {4'b1001, 2'b00, 2'b00, 3'b000, 4'b0101, 3'b000, 4'b0010, 1'b0, 6'b000000, 2'b00, 4'b0000}; // JR
                6'b001001: controls = {4'b1001, 2'b00, 2'b00, 3'b010, 4'b1001, 3'b000, 4'b0010, 1'b0, 6'b000000, 2'b00, 4'b0000}; // JALR
                default: controls = {4'b0001, 2'b00, 2'b00, 3'b000, 4'b0100, 3'b000, 4'b0010, 1'b0, 6'b000000, 2'b00, 4'b0000};   // else
            endcase
            BEQEX:   controls = {4'b0000, 2'b01, 2'b10, 3'b000, 4'b0001, 3'b000, 4'b1010, 1'b0, 6'b000000, 2'b00, 4'b0000};
            BNQEX:   controls = {4'b0000, 2'b01, 2'b11, 3'b000, 4'b0001, 3'b000, 4'b1010, 1'b0, 6'b000000, 2'b00, 4'b0000};
            ADDIEX:  controls = {4'b0000, 2'b01, 2'b00, 3'b000, 4'b0000, 3'b010, 4'b0010, 1'b0, 6'b000000, 2'b00, 4'b0000};
            ADDIWB:  controls = {4'b0001, 2'b00, 2'b00, 3'b000, 4'b0000, 3'b000, 4'b0000, 1'b0, 6'b000000, 2'b00, 4'b0000};
            LUIEX:   controls = {4'b0000, 2'b01, 2'b00, 3'b000, 4'b0000, 3'b100, 4'b0010, 1'b0, 6'b000000, 2'b00, 4'b0000};
            LUIWB:   controls = {4'b0001, 2'b00, 2'b00, 3'b000, 4'b0000, 3'b000, 4'b0000, 1'b0, 6'b000000, 2'b00, 4'b0000};
            ORIEX:   controls = {4'b0000, 2'b01, 2'b00, 3'b000, 4'b0000, 3'b101, 4'b0001, 1'b0, 6'b000000, 2'b00, 4'b0000};
            ORIWB:   controls = {4'b0001, 2'b00, 2'b00, 3'b000, 4'b0000, 3'b000, 4'b0000, 1'b0, 6'b000000, 2'b00, 4'b0000};
            SLTIEX:  controls = {4'b0000, 2'b01, 2'b00, 3'b000, 4'b0000, 3'b010, 4'b1011, 1'b0, 6'b000000, 2'b00, 4'b0000};
            SLTIWB:  controls = {4'b0001, 2'b00, 2'b00, 3'b000, 4'b0000, 3'b000, 4'b0000, 1'b0, 6'b000000, 2'b00, 4'b0000};
            JEX:     controls = {4'b1000, 2'b00, 2'b00, 3'b000, 4'b0010, 3'b000, 4'b0010, 1'b0, 6'b000000, 2'b00, 4'b0000};
            JALEX:   controls = {4'b1001, 2'b00, 2'b00, 3'b010, 4'b1010, 3'b000, 4'b0010, 1'b0, 6'b000000, 2'b00, 4'b0000};
            INWAIT:  controls = {4'b0000, 2'b00, 2'b00, 3'b000, 4'b0000, 3'b000, 4'b0000, 1'b0, 6'b000000, 2'b00, 4'b0000};
            INREADY: controls = {4'b0001, 2'b00, 2'b00, 3'b011, 4'b0000, 3'b000, 4'b0000, 1'b0, 6'b000000, 2'b00, 4'b0000};
            OUTSTART:controls = {4'b0000, 2'b00, 2'b00, 3'b000, 4'b0000, 3'b000, 4'b0000, 1'b1, 6'b000000, 2'b00, 4'b0000};
            OUTEX:   controls = {4'b0000, 2'b00, 2'b00, 3'b000, 4'b0000, 3'b000, 4'b0000, 1'b0, 6'b000000, 2'b00, 4'b0000};
            BSEX:    controls = {4'b0000, 2'b00, 2'b00, 3'b000, 4'b0001, 3'b000, 4'b0000, 1'b0, 6'b000011, 2'b00, 4'b0000};
            ADDSEX:  controls = {4'b0000, 2'b00, 2'b00, 3'b000, 4'b0000, 3'b000, 4'b0000, 1'b0, 6'b000000, 2'b00, 4'b0000};
            ADDSWB:  controls = {4'b0000, 2'b00, 2'b00, 3'b000, 4'b0000, 3'b000, 4'b0000, 1'b0, 6'b011000, 2'b01, 4'b0000};
            SUBSEX:  controls = {4'b0000, 2'b00, 2'b00, 3'b000, 4'b0000, 3'b000, 4'b0000, 1'b0, 6'b000000, 2'b00, 4'b0001};
            SUBSWB:  controls = {4'b0000, 2'b00, 2'b00, 3'b000, 4'b0000, 3'b000, 4'b0000, 1'b0, 6'b011000, 2'b01, 4'b0000};
            MULSEX:  controls = {4'b0000, 2'b00, 2'b00, 3'b000, 4'b0000, 3'b000, 4'b0000, 1'b0, 6'b000000, 2'b00, 4'b0010};
            MULSWB:  controls = {4'b0000, 2'b00, 2'b00, 3'b000, 4'b0000, 3'b000, 4'b0000, 1'b0, 6'b011000, 2'b01, 4'b0000};
            DIVSEX0: controls = {4'b0000, 2'b00, 2'b00, 3'b000, 4'b0000, 3'b000, 4'b0000, 1'b0, 6'b000000, 2'b00, 4'b0000};
            DIVSEX1: controls = {4'b0000, 2'b00, 2'b00, 3'b000, 4'b0000, 3'b000, 4'b0000, 1'b0, 6'b000000, 2'b00, 4'b0000};
            DIVSEX2: controls = {4'b0000, 2'b00, 2'b00, 3'b000, 4'b0000, 3'b000, 4'b0000, 1'b0, 6'b000000, 2'b00, 4'b0011};
            DIVSWB:  controls = {4'b0000, 2'b00, 2'b00, 3'b000, 4'b0000, 3'b000, 4'b0000, 1'b0, 6'b011000, 2'b01, 4'b0000};
            MOVSEX:  controls = {4'b0000, 2'b00, 2'b00, 3'b000, 4'b0000, 3'b000, 4'b0000, 1'b0, 6'b000000, 2'b00, 4'b0100};
            MOVSWB:  controls = {4'b0000, 2'b00, 2'b00, 3'b000, 4'b0000, 3'b000, 4'b0000, 1'b0, 6'b011000, 2'b01, 4'b0000};
            NEGSEX:  controls = {4'b0000, 2'b00, 2'b00, 3'b000, 4'b0000, 3'b000, 4'b0000, 1'b0, 6'b000000, 2'b00, 4'b0101};
            NEGSWB:  controls = {4'b0000, 2'b00, 2'b00, 3'b000, 4'b0000, 3'b000, 4'b0000, 1'b0, 6'b011000, 2'b01, 4'b0000};
            ABSSEX:  controls = {4'b0000, 2'b00, 2'b00, 3'b000, 4'b0000, 3'b000, 4'b0000, 1'b0, 6'b000000, 2'b00, 4'b0110};
            ABSSWB:  controls = {4'b0000, 2'b00, 2'b00, 3'b000, 4'b0000, 3'b000, 4'b0000, 1'b0, 6'b011000, 2'b01, 4'b0000};
            SQRTSEX0:controls = {4'b0000, 2'b00, 2'b00, 3'b000, 4'b0000, 3'b000, 4'b0000, 1'b0, 6'b000000, 2'b00, 4'b0000};
            SQRTSEX1:controls = {4'b0000, 2'b00, 2'b00, 3'b000, 4'b0000, 3'b000, 4'b0000, 1'b0, 6'b000000, 2'b00, 4'b0111};
            SQRTSWB: controls = {4'b0000, 2'b00, 2'b00, 3'b000, 4'b0000, 3'b000, 4'b0000, 1'b0, 6'b011000, 2'b01, 4'b0000};
            CEQSEX:  controls = {4'b0000, 2'b00, 2'b00, 3'b000, 4'b0000, 3'b000, 4'b0000, 1'b0, 6'b000100, 2'b00, 4'b1000};
            CLTSEX:  controls = {4'b0000, 2'b00, 2'b00, 3'b000, 4'b0000, 3'b000, 4'b0000, 1'b0, 6'b000100, 2'b00, 4'b1001};
            MEMWBS:  controls = {4'b0000, 2'b00, 2'b00, 3'b000, 4'b0000, 3'b000, 4'b0000, 1'b0, 6'b010000, 2'b00, 4'b0000};
            MEMWRS:  controls = {4'b0100, 2'b00, 2'b00, 3'b000, 4'b0000, 3'b000, 4'b0000, 1'b0, 6'b100000, 2'b00, 4'b0000};
            FTOIEX:  controls = {4'b0000, 2'b00, 2'b00, 3'b000, 4'b0000, 3'b000, 4'b0000, 1'b0, 6'b000000, 2'b00, 4'b1010};
            FTOIWB:  controls = {4'b0001, 2'b00, 2'b00, 3'b100, 4'b0000, 3'b000, 4'b0000, 1'b0, 6'b000000, 2'b00, 4'b0000};
            ITOFEX:  controls = {4'b0000, 2'b01, 2'b00, 3'b000, 4'b0000, 3'b000, 4'b0110, 1'b0, 6'b000000, 2'b00, 4'b0000};
            ITOFWB:  controls = {4'b0000, 2'b00, 2'b00, 3'b000, 4'b0000, 3'b000, 4'b0000, 1'b0, 6'b010000, 2'b10, 4'b0000};
            default: controls = {4'bxxxx, 2'bxx, 2'bxx, 3'bxxx, 4'bxxxx, 3'bxxx, 4'bxxxx, 1'bx, 6'bxxxxxx, 2'bxx, 4'bxxxx}; // should never happen
        endcase
endmodule
