module funit(
    input  logic        clk, reset,
    input  logic        fregdst, ccwrite, borc,
    input  logic [1:0]  memtofreg, 
    input  logic [3:0]  fpucontrol,
    input  logic [31:0] aluout,
    input  logic [31:0] rd1, rd2, instr, data,
    output logic        ccrd,
    output logic [4:0]  a3,
    output logic [31:0] fregwritedata, fwritedata, fpuout
);
    
    logic [31:0] a;
    logic [31:0] fpuresult;
    logic        ccwd, rd;
    logic [2:0]  cca;

    // datapath
    always_ff @(posedge clk)
        if (reset) begin
            a <= 32'b0;
            fwritedata <= 32'b0;
        end else begin
            a <= rd1;
            fwritedata <= rd2;
        end
    fpu fpu(clk, a, fwritedata, fpucontrol, fpuresult, ccwd);
    always_ff @(posedge clk)
        fpuout <= fpuresult;
    assign cca = borc ? instr[20:18] : instr[10:8];
    conditioncode cc(clk, cca, ccwd, ccwrite, ccrd);
    /* always_ff @(posedge clk) */
    /*     ccrd <= rd; */
    assign a3 = fregdst ? instr[10:6] : instr[20:16];
    assign fregwritedata = memtofreg[1] ? aluout : (memtofreg[0] ? fpuout : data);
endmodule

module fpu(
    input  logic        clk,
    input  logic [31:0] a, b,
    input  logic [3:0]  fpucontrol,
    output logic [31:0] result,
    output logic        ccwd
);

    logic        fadd_ovf;
    logic        fsub_ovf;
    logic        fmul_ovf;
    logic        fdiv_ovf;
    logic        fneg_ovf;
    logic [31:0] fadd_result;
    logic [31:0] fsub_result;
    logic [31:0] fmul_result;
    logic [31:0] fdiv_result;
    logic [31:0] fneg_result;
    logic [31:0] fabs_result;
    logic [31:0] fsqrt_result;
    logic        fless_result;
    logic [31:0] ftoi_result;

    fadd fadd(a, b, fadd_result, fadd_ovf);
    fsub fsub(a, b, fsub_result, fsub_ovf);
    fsub fneg(32'b0, b, fneg_result, fneg_ovf);
    fmul fmul(a, b, fmul_result, fmul_ovf);
    fdiv fdiv(clk, a, b, fdiv_result, fdiv_ovf);
    fabs fabs(b, fabs_result);
    fsqrt fsqrt(clk, b, fsqrt_result);
    fless fless(a, b, fless_result);
    ftoi ftoi(a, ftoi_result);

    always_comb
        case (fpucontrol)
            // 0000: add.s
            // 0001: sub.s
            // 0010: mul.s
            // 0011: div.s
            // 0100: mov.s
            // 0101: neg.s
            // 0110: abs.s
            // 0111: sqrt.s
            // 1000: c.eq.s
            // 1001: c.lt.s
            // 1001: c.le.s
            // 1010: ftoi
            4'b0000: result = fadd_result;
            4'b0001: result = fsub_result;
            4'b0010: result = fmul_result;
            4'b0011: result = fdiv_result;
            4'b0100: result = b;
            4'b0101: result = fneg_result;
            4'b0110: result = fabs_result;
            4'b0111: result = fsqrt_result;
            4'b1000: ccwd = a == b;
            4'b1001: ccwd = fless_result;
            default: result = ftoi_result;
        endcase
endmodule

module conditioncode #(parameter DEPTH = 8) (
    input  logic       clk,
    input  logic [2:0] a,
    input  logic       w,
    input  logic       we,
    output logic       r
);

    logic dist_ram[DEPTH-1:0];
    /* integer i; */
    /* initial begin */
    /*     for (i=0; i<DEPTH; i=i+1) dist_ram[i] = 1'b0; */
    /* end */

    always_ff @(posedge clk) begin
        if (we) dist_ram[a] <= w;
    end

    assign r = dist_ram[a];

endmodule
