#
# KCU105 RevD XDC
#
## Bank  66 VCCO - VADJ_1V8_FPGA_10A - IO_L12P_T1U_N10_GC_66
set_property IOSTANDARD LVDS [get_ports CLK_125MHZ_P]
## Bank  66 VCCO - VADJ_1V8_FPGA_10A - IO_L12N_T1U_N11_GC_66
set_property PACKAGE_PIN G10 [get_ports CLK_125MHZ_P]
set_property PACKAGE_PIN F10 [get_ports CLK_125MHZ_N]
set_property IOSTANDARD LVDS [get_ports CLK_125MHZ_N]
## Bank  84 VCCO -          - IO_L22P_T3U_N6_DBC_AD0P_64
set_property PACKAGE_PIN AN8 [get_ports CPU_RESET]
set_property IOSTANDARD LVCMOS18 [get_ports CPU_RESET]
## Bank  95 VCCO -          - IO_L3P_T0L_N4_AD15P_A26_65
set_property PACKAGE_PIN K26 [get_ports USB_UART_TX]
set_property IOSTANDARD LVCMOS18 [get_ports USB_UART_TX]
## Bank  95 VCCO -          - IO_L2P_T0L_N2_FOE_B_65
set_property PACKAGE_PIN G25 [get_ports USB_UART_RX]
set_property IOSTANDARD LVCMOS18 [get_ports USB_UART_RX]


create_debug_core u_ila_0 ila
set_property ALL_PROBE_SAME_MU true [get_debug_cores u_ila_0]
set_property ALL_PROBE_SAME_MU_CNT 1 [get_debug_cores u_ila_0]
set_property C_ADV_TRIGGER false [get_debug_cores u_ila_0]
set_property C_DATA_DEPTH 8192 [get_debug_cores u_ila_0]
set_property C_EN_STRG_QUAL false [get_debug_cores u_ila_0]
set_property C_INPUT_PIPE_STAGES 0 [get_debug_cores u_ila_0]
set_property C_TRIGIN_EN false [get_debug_cores u_ila_0]
set_property C_TRIGOUT_EN false [get_debug_cores u_ila_0]
set_property port_width 1 [get_debug_ports u_ila_0/clk]
connect_debug_port u_ila_0/clk [get_nets [list cw/inst/clk_out1]]
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe0]
set_property port_width 32 [get_debug_ports u_ila_0/probe0]
connect_debug_port u_ila_0/probe0 [get_nets [list {dut/core/fwd3[0]} {dut/core/fwd3[1]} {dut/core/fwd3[2]} {dut/core/fwd3[3]} {dut/core/fwd3[4]} {dut/core/fwd3[5]} {dut/core/fwd3[6]} {dut/core/fwd3[7]} {dut/core/fwd3[8]} {dut/core/fwd3[9]} {dut/core/fwd3[10]} {dut/core/fwd3[11]} {dut/core/fwd3[12]} {dut/core/fwd3[13]} {dut/core/fwd3[14]} {dut/core/fwd3[15]} {dut/core/fwd3[16]} {dut/core/fwd3[17]} {dut/core/fwd3[18]} {dut/core/fwd3[19]} {dut/core/fwd3[20]} {dut/core/fwd3[21]} {dut/core/fwd3[22]} {dut/core/fwd3[23]} {dut/core/fwd3[24]} {dut/core/fwd3[25]} {dut/core/fwd3[26]} {dut/core/fwd3[27]} {dut/core/fwd3[28]} {dut/core/fwd3[29]} {dut/core/fwd3[30]} {dut/core/fwd3[31]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe1]
set_property port_width 14 [get_debug_ports u_ila_0/probe1]
connect_debug_port u_ila_0/probe1 [get_nets [list {dut/core/pc[2]} {dut/core/pc[3]} {dut/core/pc[4]} {dut/core/pc[5]} {dut/core/pc[6]} {dut/core/pc[7]} {dut/core/pc[8]} {dut/core/pc[9]} {dut/core/pc[10]} {dut/core/pc[11]} {dut/core/pc[12]} {dut/core/pc[13]} {dut/core/pc[14]} {dut/core/pc[15]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe2]
set_property port_width 32 [get_debug_ports u_ila_0/probe2]
connect_debug_port u_ila_0/probe2 [get_nets [list {dut/core/rd1[0]} {dut/core/rd1[1]} {dut/core/rd1[2]} {dut/core/rd1[3]} {dut/core/rd1[4]} {dut/core/rd1[5]} {dut/core/rd1[6]} {dut/core/rd1[7]} {dut/core/rd1[8]} {dut/core/rd1[9]} {dut/core/rd1[10]} {dut/core/rd1[11]} {dut/core/rd1[12]} {dut/core/rd1[13]} {dut/core/rd1[14]} {dut/core/rd1[15]} {dut/core/rd1[16]} {dut/core/rd1[17]} {dut/core/rd1[18]} {dut/core/rd1[19]} {dut/core/rd1[20]} {dut/core/rd1[21]} {dut/core/rd1[22]} {dut/core/rd1[23]} {dut/core/rd1[24]} {dut/core/rd1[25]} {dut/core/rd1[26]} {dut/core/rd1[27]} {dut/core/rd1[28]} {dut/core/rd1[29]} {dut/core/rd1[30]} {dut/core/rd1[31]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe3]
set_property port_width 32 [get_debug_ports u_ila_0/probe3]
connect_debug_port u_ila_0/probe3 [get_nets [list {dut/core/rd2[0]} {dut/core/rd2[1]} {dut/core/rd2[2]} {dut/core/rd2[3]} {dut/core/rd2[4]} {dut/core/rd2[5]} {dut/core/rd2[6]} {dut/core/rd2[7]} {dut/core/rd2[8]} {dut/core/rd2[9]} {dut/core/rd2[10]} {dut/core/rd2[11]} {dut/core/rd2[12]} {dut/core/rd2[13]} {dut/core/rd2[14]} {dut/core/rd2[15]} {dut/core/rd2[16]} {dut/core/rd2[17]} {dut/core/rd2[18]} {dut/core/rd2[19]} {dut/core/rd2[20]} {dut/core/rd2[21]} {dut/core/rd2[22]} {dut/core/rd2[23]} {dut/core/rd2[24]} {dut/core/rd2[25]} {dut/core/rd2[26]} {dut/core/rd2[27]} {dut/core/rd2[28]} {dut/core/rd2[29]} {dut/core/rd2[30]} {dut/core/rd2[31]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe4]
set_property port_width 32 [get_debug_ports u_ila_0/probe4]
connect_debug_port u_ila_0/probe4 [get_nets [list {dut/core/frd1[0]} {dut/core/frd1[1]} {dut/core/frd1[2]} {dut/core/frd1[3]} {dut/core/frd1[4]} {dut/core/frd1[5]} {dut/core/frd1[6]} {dut/core/frd1[7]} {dut/core/frd1[8]} {dut/core/frd1[9]} {dut/core/frd1[10]} {dut/core/frd1[11]} {dut/core/frd1[12]} {dut/core/frd1[13]} {dut/core/frd1[14]} {dut/core/frd1[15]} {dut/core/frd1[16]} {dut/core/frd1[17]} {dut/core/frd1[18]} {dut/core/frd1[19]} {dut/core/frd1[20]} {dut/core/frd1[21]} {dut/core/frd1[22]} {dut/core/frd1[23]} {dut/core/frd1[24]} {dut/core/frd1[25]} {dut/core/frd1[26]} {dut/core/frd1[27]} {dut/core/frd1[28]} {dut/core/frd1[29]} {dut/core/frd1[30]} {dut/core/frd1[31]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe5]
set_property port_width 32 [get_debug_ports u_ila_0/probe5]
connect_debug_port u_ila_0/probe5 [get_nets [list {dut/core/frd2[0]} {dut/core/frd2[1]} {dut/core/frd2[2]} {dut/core/frd2[3]} {dut/core/frd2[4]} {dut/core/frd2[5]} {dut/core/frd2[6]} {dut/core/frd2[7]} {dut/core/frd2[8]} {dut/core/frd2[9]} {dut/core/frd2[10]} {dut/core/frd2[11]} {dut/core/frd2[12]} {dut/core/frd2[13]} {dut/core/frd2[14]} {dut/core/frd2[15]} {dut/core/frd2[16]} {dut/core/frd2[17]} {dut/core/frd2[18]} {dut/core/frd2[19]} {dut/core/frd2[20]} {dut/core/frd2[21]} {dut/core/frd2[22]} {dut/core/frd2[23]} {dut/core/frd2[24]} {dut/core/frd2[25]} {dut/core/frd2[26]} {dut/core/frd2[27]} {dut/core/frd2[28]} {dut/core/frd2[29]} {dut/core/frd2[30]} {dut/core/frd2[31]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe6]
set_property port_width 32 [get_debug_ports u_ila_0/probe6]
connect_debug_port u_ila_0/probe6 [get_nets [list {dut/core/wd3[0]} {dut/core/wd3[1]} {dut/core/wd3[2]} {dut/core/wd3[3]} {dut/core/wd3[4]} {dut/core/wd3[5]} {dut/core/wd3[6]} {dut/core/wd3[7]} {dut/core/wd3[8]} {dut/core/wd3[9]} {dut/core/wd3[10]} {dut/core/wd3[11]} {dut/core/wd3[12]} {dut/core/wd3[13]} {dut/core/wd3[14]} {dut/core/wd3[15]} {dut/core/wd3[16]} {dut/core/wd3[17]} {dut/core/wd3[18]} {dut/core/wd3[19]} {dut/core/wd3[20]} {dut/core/wd3[21]} {dut/core/wd3[22]} {dut/core/wd3[23]} {dut/core/wd3[24]} {dut/core/wd3[25]} {dut/core/wd3[26]} {dut/core/wd3[27]} {dut/core/wd3[28]} {dut/core/wd3[29]} {dut/core/wd3[30]} {dut/core/wd3[31]}]]
set_property C_CLK_INPUT_FREQ_HZ 300000000 [get_debug_cores dbg_hub]
set_property C_ENABLE_CLK_DIVIDER false [get_debug_cores dbg_hub]
set_property C_USER_SCAN_CHAIN 1 [get_debug_cores dbg_hub]
connect_debug_port dbg_hub/clk [get_nets clk]
