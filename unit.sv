module unit(
    input  logic        clk, reset,
    input  logic [31:0] pc,
    input  logic [4:0]  shamt,
    input  logic [31:0] uart_data,
    input  logic [31:0] rd1, rd2,
    input  logic [1:0]  alusrca,
    input  logic [2:0]  memtoreg,
    input  logic [1:0]  regdst, pcsrc, 
    input  logic [2:0]  alusrcb,
    input  logic [3:0]  alucontrol,
    input  logic [31:0] fpuout,
    output logic [4:0]  writereg,
    output logic        zero,
    output logic [31:0] wd3,
    output logic [31:0] pcnext, aluout, writedata, 
    input  logic [25:0] instr, 
    input  logic [31:0] data
    );

    // Below are the internal signals of the datapath module.
    logic [31:0] srca, srcb;
    logic [31:0] aluresult;
    logic [31:0] a;
    logic [31:0] signimm;   // the sign-extended immediate

    // datapath
    mux3    #(5)  regdstmux(instr[20:16], instr[15:11], 5'd31, regdst, writereg);
    mux5    #(32) wdmux(aluout, data, pc, uart_data, fpuout, memtoreg, wd3);
    signext       se(instr[15:0], signimm);
    flopr   #(32) areg(clk, reset, rd1, a);
    flopr   #(32) breg(clk, reset, rd2, writedata);
    mux3    #(32) srcamux(pc, a, {27'b0, shamt}, alusrca, srca);
    mux6    #(32) srcbmux(writedata, 32'b100, signimm, {signimm[29:0], 2'b00}, {instr[15:0], 16'b0}, {16'b0, instr[15:0]}, alusrcb, srcb);
    alu           alu(srca, srcb, alucontrol, aluresult, zero);
    flopr   #(32) alureg(clk, reset, aluresult, aluout);
    mux3    #(32) pcmux(aluresult, aluout, {pc[31:28], instr[25:0], 2'b00}, pcsrc, pcnext);
endmodule

module signext(
    input  logic [15:0] a,
    output logic [31:0] y);
              
    assign y = {{16{a[15]}}, a};
endmodule

module flopr #(parameter WIDTH = 8) (
    input  logic             clk, reset,
    input  logic [WIDTH-1:0] d, 
    output logic [WIDTH-1:0] q);

    always_ff @(posedge clk)
      if (reset) q <= 0;
      else       q <= d;
endmodule

/* module mux2 #(parameter WIDTH = 8) ( */
/*     input  logic [WIDTH-1:0] d0, d1, */ 
/*     input  logic             s, */ 
/*     output logic [WIDTH-1:0] y); */

/*     assign y = s ? d1 : d0; */ 
/* endmodule */

module alu(
    input  logic [31:0] a, b,
    input  logic [3:0]  alucontrol,
    output logic [31:0] result,
    output logic        zero);

    logic [31:0] condinvb, sum, itof_result;

    assign condinvb = alucontrol[3] ? ~b : b;
    assign sum = a + condinvb + {31'b0, alucontrol[3]};
    itof itof(a, itof_result);
    always_comb
      case (alucontrol[2:0])
        3'b000: result = a & b;
        3'b001: result = a | b;
        3'b010: result = sum;
        3'b011: result = {31'b0, sum[31]};
        3'b100: result = b << a[4:0];
        3'b101: result = b >> a[4:0];
        default: result = itof_result;
      endcase

    assign zero = (result == 32'b0);
endmodule

module mux3 #(parameter WIDTH = 8) (
    input  logic [WIDTH-1:0] d0, d1, d2,
    input  logic [1:0]       s, 
    output logic [WIDTH-1:0] y);

    assign y = s[1] ? d2 : (s[0] ? d1 : d0); 
endmodule

/* module mux4 #(parameter WIDTH = 8) ( */
/*     input  logic [WIDTH-1:0] d0, d1, d2, d3, */
/*     input  logic [1:0]       s, */ 
/*     output logic [WIDTH-1:0] y); */

/*     always_comb */
/*         case(s) */
/*             2'b00: y = d0; */
/*             2'b01: y = d1; */
/*             2'b10: y = d2; */
/*             2'b11: y = d3; */
/*         endcase */
/* endmodule */

module mux5 #(parameter WIDTH = 8) (
    input  logic [WIDTH-1:0] d0, d1, d2, d3, d4,
    input  logic [2:0]       s, 
    output logic [WIDTH-1:0] y);

    always_comb
        case(s)
            3'b000: y = d0;
            3'b001: y = d1;
            3'b010: y = d2;
            3'b011: y = d3;
            default: y = d4;
        endcase
endmodule

module mux6 #(parameter WIDTH = 8) (
    input  logic [WIDTH-1:0] d0, d1, d2, d3, d4, d5,
    input  logic [2:0]       s, 
    output logic [WIDTH-1:0] y);

    always_comb
        case(s)
            3'b000: y = d0;
            3'b001: y = d1;
            3'b010: y = d2;
            3'b011: y = d3;
            3'b100: y = d4;
            default: y = d5;
        endcase
endmodule
