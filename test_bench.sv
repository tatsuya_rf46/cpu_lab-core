module testbench();

    logic clk;
    logic rst;
    logic reset;

    logic uart_rx;
    logic uart_tx;

    logic [7:0] uart_buffer;
    logic ready;
    uart_rx #(434) ur(uart_buffer, ready, uart_tx, clk, rst);
    always @(negedge clk)
      begin
        if(ready) begin
          $write("%c", uart_buffer);
        end
      end

    // initialize test
    initial
        begin
          rst = 1; # 22; rst = 0;
        end

    // generate clock to sequence tests
    always
        begin
            clk = 1; # 5; clk = 0; # 5;
        end

    // uart in
    initial
        begin
            // 32bit 10
            uart_rx = 1;
            # 100;
            uart_rx = 0;
            # 8680;
            uart_rx = 0;
            # 8680;
            uart_rx = 1;
            # 8680;
            uart_rx = 0;
            # 8680;
            uart_rx = 1;
            # 8680;
            uart_rx = 0;
            # 8680;
            uart_rx = 0;
            # 8680;
            uart_rx = 0;
            # 8680;
            uart_rx = 0;
            # 8680;
            uart_rx = 1;
            # 17360;
            uart_rx = 0;
            # 8680;
            uart_rx = 0;
            # 8680;
            uart_rx = 0;
            # 8680;
            uart_rx = 0;
            # 8680;
            uart_rx = 0;
            # 8680;
            uart_rx = 0;
            # 8680;
            uart_rx = 0;
            # 8680;
            uart_rx = 0;
            # 8680;
            uart_rx = 0;
            # 8680;
            uart_rx = 1;
            # 17360;
            uart_rx = 0;
            # 8680;
            uart_rx = 0;
            # 8680;
            uart_rx = 0;
            # 8680;
            uart_rx = 0;
            # 8680;
            uart_rx = 0;
            # 8680;
            uart_rx = 0;
            # 8680;
            uart_rx = 0;
            # 8680;
            uart_rx = 0;
            # 8680;
            uart_rx = 0;
            # 8680;
            uart_rx = 1;
            # 17360;
            uart_rx = 0;
            # 8680;
            uart_rx = 0;
            # 8680;
            uart_rx = 0;
            # 8680;
            uart_rx = 0;
            # 8680;
            uart_rx = 0;
            # 8680;
            uart_rx = 0;
            # 8680;
            uart_rx = 0;
            # 8680;
            uart_rx = 0;
            # 8680;
            uart_rx = 0;
            # 8680;
            uart_rx = 1;
        end

    always_ff @(posedge clk)
        reset <= rst;

    // instantiate device to be tested
    top dut(clk, reset, uart_rx, uart_tx);
  
    // check results
    /* always @(negedge clk) */
    /*   begin */
    /*     if(memwrite) begin */
    /*       if(dataadr === 84 & writedata === 7) begin */
    /*         $display("Simulation succeeded"); */
    /*         $stop; */
    /*       end else if (dataadr !== 80) begin */
    /*         $display("Simulation failed"); */
    /*         $stop; */
    /*       end */
    /*     end */
    /*   end */
endmodule
