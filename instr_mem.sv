module instr_mem (clk, en, rst, addr, dout);
    input clk;
    input en;
    input rst;
    input [31:0] addr;
    output [31:0] dout;

    (* ram_style = "BLOCK" *) reg [31:0] RAM [50000:0];
    reg [31:0] dout;

    initial begin
        $readmemb("mytest2_instr.mem", RAM, 0, 50000);
    end
    
    always @(posedge clk)
    begin
        if (en) //optional enable
        begin
            if (rst) //optional reset
                dout <= 0;
            else
            dout <= RAM[{2'b00, addr[31:2]}];
        end
    end
endmodule
