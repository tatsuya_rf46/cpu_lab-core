module uart_unit #(CLK_PER_HALF_BIT = 434) (
    input wire  clk, rstn,
    input wire  rxd,
    output logic txd,
    input wire  tx_start,
    input wire [7:0] rd,
    output logic rx_ready,
    output logic tx_busy,
    output logic [31:0] wd
);

    logic [31:8] buffer;
    logic [7:0] rdata;
    always_ff @(posedge clk)
        if (rstn) buffer <= 24'b0;
        else if (rx_ready) wd <= {buffer[31:8], rdata};

    uart_rx #(CLK_PER_HALF_BIT) u2(rdata, rx_ready, rxd, clk, rstn);
    uart_tx #(CLK_PER_HALF_BIT) u1(rd[7:0], tx_start, tx_busy, txd, clk, rstn);
endmodule

module uart_rx #(CLK_PER_HALF_BIT = 217) (
    output logic [7:0] rdata,
    output logic       rdata_ready,
    input wire         rxd,
    input wire         clk,
    input wire         rstn
);

    localparam e_clk_half_bit = CLK_PER_HALF_BIT - 1;

    localparam e_clk_bit = CLK_PER_HALF_BIT * 2 - 1;

    localparam e_clk_stop_bit = CLK_PER_HALF_BIT*2*9/10 - 1;

    logic [3:0]  status;
    logic [31:0] counter;
    logic        wait_stop_bit;
    logic        rst_ctr;
    logic        next;

    (* ASYNC_REG = "true" *) reg [2:0] sync_reg;

    localparam s_idle = 0;
    localparam s_wait_start = 1;
    localparam s_start_bit = 2;
    localparam s_bit_0 = 3;
    localparam s_bit_1 = 4;
    localparam s_bit_2 = 5;
    localparam s_bit_3 = 6;
    localparam s_bit_4 = 7;
    localparam s_bit_5 = 8;
    localparam s_bit_6 = 9;
    localparam s_bit_7 = 10;
    localparam s_stop_bit = 11;

    always @(posedge clk) begin
        if (rstn) begin
            counter <= 0;
            next <= 0;
            wait_stop_bit <= 0;
        end else begin
            if (counter == e_clk_bit || rst_ctr) begin
                counter <= 0;
            end else begin
                counter <= counter + 1;
            end
            if (~rst_ctr && counter == e_clk_bit) begin
                next <= 1;
            end else begin
                next <= 0;
            end
            if (~rst_ctr && counter == e_clk_half_bit) begin
                wait_stop_bit <= 1;
            end else begin
                wait_stop_bit <= 0;
            end
        end
    end

    always @(posedge clk) begin
        if (rstn) begin
            rdata <= 8'b0;
            status <= s_idle;
            rst_ctr <= 0;
            sync_reg <= 3'b111;
        end else begin
            rst_ctr <= 0;
            rdata_ready <= 0;

            sync_reg[0] <= rxd;
            sync_reg[2:1] <= sync_reg[1:0];

            if (status == s_idle) begin
                if (~sync_reg[2]) begin
                    status <= s_wait_start;
                    rst_ctr <= 1;
                end
            end else if (status == s_wait_start) begin
                if (wait_stop_bit) begin
                    status <= s_start_bit;
                    rst_ctr <= 1;
                end
            end else if (next) begin
                if (status == s_bit_7) begin
                    if (sync_reg[2]) rdata_ready <= 1;
                    status <= s_idle;
                end else begin
                    rdata[7] <= sync_reg[2];
                    rdata[6:0] <= rdata[7:1];
                    status <= status + 1;
                end
            end
        end
    end
endmodule

module uart_tx #(CLK_PER_HALF_BIT = 217) (
    input wire [7:0] sdata,
    input wire       tx_start,
    output logic     tx_busy,
    output logic     txd,
    input wire       clk,
    input wire       rstn
);

    localparam e_clk_bit = CLK_PER_HALF_BIT * 2 - 1;

    localparam e_clk_stop_bit = (CLK_PER_HALF_BIT*2*9)/10 - 1;

    logic [7:0]      txbuf;
    logic [3:0]      status;
    logic [31:0]     counter;
    logic            next;
    logic            fin_stop_bit;
    logic            rst_ctr;

    localparam s_idle = 0;
    localparam s_start_bit = 1;
    localparam s_bit_0 = 2;
    localparam s_bit_1 = 3;
    localparam s_bit_2 = 4;
    localparam s_bit_3 = 5;
    localparam s_bit_4 = 6;
    localparam s_bit_5 = 7;
    localparam s_bit_6 = 8;
    localparam s_bit_7 = 9;
    localparam s_stop_bit = 10;

    // generate event signal
    always @(posedge clk) begin
        if (rstn) begin
            counter <= 0;
            next <= 0;
            fin_stop_bit <=0;
        end else begin
            if (counter == e_clk_bit || rst_ctr) begin
                counter <= 0;
            end else if (status != s_idle) begin
                counter <= counter + 1;
            end
            if (~rst_ctr && counter == e_clk_bit) begin
                next <= 1;
            end else begin
                next <= 0;
            end
            if (~rst_ctr && counter == e_clk_stop_bit) begin
                fin_stop_bit <= 1;
            end else begin
                fin_stop_bit <= 0;
            end
        end
    end

    always @(posedge clk) begin
        if (rstn) begin
            txbuf <= 8'b0;
            status <= s_idle;
            rst_ctr <= 0;
            txd <= 1;
            tx_busy <= 0;
        end else begin
            rst_ctr <= 0;

            if (status == s_idle) begin
                if (tx_start) begin
                    txbuf <= sdata;
                    status <= s_start_bit;
                    rst_ctr <= 1;
                    txd <= 0;
                    tx_busy <= 1;
                end
            end else if (status == s_stop_bit) begin
                if (fin_stop_bit) begin
                    txd <= 1;
                    status <= s_idle;
                    tx_busy <= 0;
                end
            end else if (next) begin
                if (status == s_bit_7) begin
                    txd <= 1;
                    status <= s_stop_bit;
                end else begin
                    txd <= txbuf[0];
                    txbuf <= txbuf >> 1;
                    status <= status + 1;
                end
            end
        end
    end
endmodule // uart_tx
