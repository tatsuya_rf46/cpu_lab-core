module top(
    input  logic        clk, reset, 
    input  logic        uart_rx,
    output logic        uart_tx
);

    logic memwrite, irwrite;
    logic [31:0] instr, data;
    logic [31:0] writedata, pc, aluout;
  
    // Core
    core core(clk, reset, uart_rx, uart_tx, pc, aluout, writedata, memwrite, irwrite, instr, data);

    // Memory 
    instr_mem im(clk, irwrite, reset, pc, instr);
    data_mem dm(clk, memwrite, reset, aluout, writedata, data);
endmodule

module core(
    input  logic        clk, reset,
    input  logic        uart_rx,
    output logic        uart_tx,
    output logic [31:0] pc, aluout, writedata,
    output logic        memwrite, irwrite,
    input  logic [31:0] instr, data
);

    logic        zero, pcen, regwrite;
    logic [1:0]  alusrca;
    logic        rx_ready, tx_busy, tx_start;
    logic [2:0]  memtoreg;
    logic [1:0]  regdst, pcsrc;
    logic [2:0]  alusrcb;
    logic [3:0]  alucontrol;
    logic [31:0] pcnext;
    logic [31:0] unit_writedata;
    logic [31:0] wd3, rd1, rd2;
    logic [4:0]  writereg;
    logic [31:0] uart_data;
    logic        iorf;
    logic        fregwrite, fregdst, ccwrite, borc;
    logic [1:0]  memtofreg;
    logic        ccrd;
    logic [3:0]  fpucontrol;
    logic [4:0]  writefreg;
    logic [31:0] fwd3, frd1, frd2;
    logic [31:0] funit_writedata, fpuout;

    // PC register
    flopenr #(32) pcreg(clk, reset, pcen, pcnext, pc);

    // Write Data
    assign writedata = iorf ? funit_writedata : unit_writedata; 

    // Controller
    controller c(
        clk, reset,
        instr[31:26], instr[5:0], instr[25:21],
        zero,
        rx_ready, tx_busy,
        ccrd, instr[16:16],
        pcen, memwrite, irwrite, regwrite,
        alusrca, 
        memtoreg, regdst, pcsrc,
        alusrcb, alucontrol,
        tx_start,
        iorf,
        fregwrite, fregdst, ccwrite, borc,
        memtofreg,
        fpucontrol
    );

    // UART Unit
    uart_unit uu(
        clk, reset,
        uart_rx,
        uart_tx,
        tx_start,
        unit_writedata[7:0],
        rx_ready, tx_busy, uart_data
    );

    // Int Register
    regfile intreg(clk, regwrite, instr[25:21], instr[20:16], writereg, wd3, rd1, rd2);

    // Load, Store, and Integer Unit
    unit u(
        clk, reset, 
        pc, 
        instr[10:6],
        uart_data,
        rd1, rd2,
        alusrca,
        memtoreg,
        regdst, pcsrc,
        alusrcb, alucontrol,
        fpuout,
        writereg,
        zero,
        wd3,
        pcnext, aluout, unit_writedata,
        instr[25:0], data
    );

    // Float Register
    regfile floatreg(clk, fregwrite, instr[15:11], instr[20:16], writefreg, fwd3, frd1, frd2);

    // Floating Unit
    funit f(
        clk, reset,
        fregdst, ccwrite, borc,
        memtofreg,
        fpucontrol,
        aluout,
        frd1, frd2, instr,
        data,
        ccrd,
        writefreg,
        fwd3, funit_writedata, fpuout
    );
endmodule

module flopenr #(parameter WIDTH = 8) (
    input  logic             clk, reset, en,
    input  logic [WIDTH-1:0] d, 
    output logic [WIDTH-1:0] q
);

    always_ff @(posedge clk)
        if (reset)   q <= 0;
        else if (en) q <= d;
endmodule

// Register File
module regfile #(parameter DEPTH = 32) (
    input  logic        clk, 
    input  logic        we3, 
    input  logic [4:0]  ra1, ra2, wa3, 
    input  logic [DEPTH-1:0] wd3, 
    output logic [DEPTH-1:0] rd1, rd2
);

    logic [DEPTH-1:0] dist_ram[DEPTH-1:0];
    integer i;
    initial begin
        for (i=0; i<DEPTH; i=i+1) dist_ram[i] = 32'b0;
        //debug
        /* dist_ram[0] = 32'b0; */
        /* dist_ram[1] = 32'b0; */
        /* dist_ram[2] = 32'b0; */
        /* dist_ram[3] = 32'h40000000; */
        /* dist_ram[4] = 32'h40400000; */
        /* for (i=5; i<DEPTH; i=i+1) dist_ram[i] = 32'b0; */
    end

    // three ported register file
    // read two ports combinationally
    // write third port on rising edge of clk
    // register 0 hardwired to 0
    // note: for pipelined processor, write third port
    // on falling edge of clk
  
    always_ff @(posedge clk)
      if (we3) dist_ram[wa3] <= wd3;	
  
    /* assign rd1 = (ra1 != 0) ? dist_ram[ra1] : 0; */
    /* assign rd2 = (ra2 != 0) ? dist_ram[ra2] : 0; */
    assign rd1 = dist_ram[ra1];
    assign rd2 = dist_ram[ra2];
endmodule
